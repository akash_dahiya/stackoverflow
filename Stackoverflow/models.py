from django.db import models

# Parent Model
class Parent(models.Model):
	description = models.TextField()
	upvote = models.IntegerField(default=0)
	downvote = models.IntegerField(default=0)
	created_by = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_by = models.CharField(max_length=255,blank=True,null=True)
	updated_at = models.DateTimeField(auto_now=True)


# Question Model
class Question(Parent):
	title = models.CharField(max_length=255)
	tags = models.TextField()
	def __unicode__(self):
		return str(self.title)+ ' ' +str(self.description)+ ' ' +str(self.tags)


# Answer Model
class Answer(Parent):
	question = models.ForeignKey(Question)
	def __unicode__(self):
		return str(self.question)+ ' ' +str(self.description)+ ' ' +str(self.created_by)


