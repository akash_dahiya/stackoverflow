from django.contrib import admin
from django.conf.urls import include, url
from Stackoverflow.views import (QuestionView, AnswerView, TagView,
								QuestionUpvote, QuestionDownvote,
								AnswerUpvote, AnswerDownvote, AllAnswer,
								AnswerGroupedByQuestion, AnswersList,
								PopularQuestion)
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
	url(r'^question/$',csrf_exempt(QuestionView.as_view())),
	url(r'^question/(?P<id>\d+)/$',csrf_exempt(QuestionView.as_view())),
	url(r'^answer/(?P<id>\d+)/$',csrf_exempt(AnswerView.as_view())),
	url(r'^tags/(?P<tag>\w+)/$',csrf_exempt(TagView.as_view())),
	url(r'^question_upvote/(?P<id>\d+)/$',csrf_exempt(QuestionUpvote.as_view())),
	url(r'^question_downvote/(?P<id>\d+)/$',csrf_exempt(QuestionDownvote.as_view())),
	url(r'^answer_upvote/(?P<id>\d+)/$',csrf_exempt(AnswerUpvote.as_view())),
	url(r'^answer_downvote/(?P<id>\d+)/$',csrf_exempt(AnswerDownvote.as_view())),
	url(r'^answers/(?P<id>\d+)/$',csrf_exempt(AllAnswer.as_view())),
	url(r'^grouped_answers/(?P<id>\d+)/$',csrf_exempt(AnswerGroupedByQuestion.as_view())),
	url(r'^answer_list/(?P<id>\d+)/$',csrf_exempt(AnswersList.as_view())),
	url(r'^popular_question/$',csrf_exempt(PopularQuestion.as_view()))
]
