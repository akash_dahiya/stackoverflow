# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('Stackoverflow', '0002_remove_parent_updated_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='parent',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2018, 7, 18, 7, 15, 50, 962012, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
