from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, QueryDict
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F,Count
import json
from .models import Question, Answer

#for getting info about questions in stackoverflow
class QuestionView(View):

	#get: to fetch question
	def get(self, request, id=None):
		try:
			question = Question.objects.get(id=id)
			res={}
			res['title'] =question.title
			res['description'] = question.description
			res['created_by'] = question.created_by
			res['created_at'] = str(question.created_at)
			res['updated_by'] = question.updated_by
			res['updated_at'] = str(question.updated_at)
			res['tags'] = question.tags
			return HttpResponse(json.dumps(res), content_type='application/json', status=200) 
		except ObjectDoesNotExist:
			return HttpResponse("Question not avaiable")

	#post: to add a question		
	def post(self, request):
		data = request.POST
		add_question = Question(title=data.get('title'),
								description=data.get('description'),
								created_by=data.get('created_by'),
								updated_by=data.get('updated_by'),
								tags=data.get('tags'))
		add_question.save()
		return HttpResponse("Question added successfully")	

	#put: to update a question	
	def put(self, request, id):
		data = QueryDict(request.body)
		try:
			update_ques = Question.objects.get(id=id)
			if data.get('description') is not None:
					update_ques.description = data.get('description')
			if data.get('updated_by') is not None:
					update_ques.updated_by = data.get('updated_by')
			update_ques.save()
			return HttpResponse(" Question updated successfully")
		except 	ObjectDoesNotExist:
			return("Question not avaiable")

	#delete: to delete a question		
	def delete(self, request, id):
		try:
			delete_question = Question.objects.get(id=id)
			delete_question.delete()
			return HttpResponse("Question deleted successfully")
		except ObjectDoesNotExist:
			return HttpResponse("Question doesn't exist")	


#for getting info about the answer in stackoverflow
class AnswerView(View):

	#post: add a answer to a question
	def post(self, request, id):
		data = request.POST
		try:
			question = Question.objects.get(id=id)
			add_answer = Answer(description=data.get('description'), created_by=data.get('created_by'))
			add_answer.question = question
			add_answer.save()
			return HttpResponse("Answer added successfully")
		except ObjectDoesNotExist:
			return HttpResponse("No Question is Avaiable")

	#put: to update a answer	
	def put(self, request, id):
		data = QueryDict(request.body)
		try:
			update_ans = Answer.objects.get(id=id)
			if data.get('description') is not None:
				update_ans.description = data.get('description')
			if data.get('updated_by') is not None:
				update_ans.updated_by = data.get('updated_by')
			update_ans.save()
			return HttpResponse("Answer is updated")
		except ObjectDoesNotExist:
			return("Answer not avaiable")

	#delete: to delete a answer
	def delete(self, request, id):
		try:
			delete_answer=Answer.objects.get(id=id)
			delete_answer.delete()
			return HttpResponse("Answer deleted successfully")
		except ObjectDoesNotExist:
			return HttpResponse("Answer doesn't exist")	


# for getting all question related to a tag
class TagView(View):

	#get: to fetch all question related to a question 
	def get(self, request, tag):
			ques_tag = Question.objects.filter(tags__icontains=tag)
			if ques_tag:
				ques = []
				for i in ques_tag:
					ques.append(i.description)	
				return HttpResponse(ques, status=200)
			return HttpResponse("This tag is not related to any question")

# for upvote of a question
class QuestionUpvote(View):

	#put: to update the upvote of a question
	def put(self, request, id):
		try:
			update_vote_2 = Question.objects.filter(id=id).update(upvote=F('upvote') + 1)
			return HttpResponse("Upvoted")
		except ObjectDoesNotExist:
			return HttpResponse("No question exist")


# for downvote of a question
class QuestionDownvote(View):

	#put: to update the downvote of a question
	def put(self, request, id):
		try:
			update_vote = Question.objects.filter(id=id).update(upvote=F('downvote') - 1)
			return HttpResponse("Downvoted")
		except ObjectDoesNotExist:
			return HttpResponse("No question exist")

#for upvote of a answer
class AnswerUpvote(View):

	#put: to update the upvote of answer
	#Can be done in one query
	def put(self, request, id):
		try:
			update_vote = Answer.objects.filter(id=id).update(upvote=F('upvote') + 1)
			return HttpResponse("Upvoted")
		except ObjectDoesNotExist:
			return HttpResponse("No question exist")


#for downvote of a answer
class AnswerDownvote(View):

	#put: to update the downvote a answer
	def put(self, request, id):
		try:
			update_vote = Answer.objects.filter(id=id).update(upvote=F('downvote') - 1)
			return HttpResponse("downvoted")
		except ObjectDoesNotExist:
			return HttpResponse("No question exist")

#Answers of a question
class AllAnswer(View):

	#get: to fetch all the answer to a question
	def get(self, request, id):
		try:
			allanswer = Answer.objects.filter(question=id)
			print(allanswer)
			ans = []
			for i in allanswer:
				ans.append(i.description)
			return HttpResponse(ans,status=200)
		except ObjectDoesNotExist:
			return HttpResponse("No answer is releted to this question")	

#Answers grouped by question
class AnswerGroupedByQuestion(View):

	#get: to fetch question and its all answer
	def get(self, request, id):
		all_answer = Answer.objects.filter(question_id=id)
		if all_answer:
			ans_list = []
			for ans in all_answer:
				data = {}
				data['answer_id'] = ans.id
				data['description'] = ans.description
				ans_list.append(data)
			ques = Question.objects.get(id=id)
			grp_data = {}
			grp_data['question_id'] = id
			grp_data['title'] = ques.title
			grp_data['answers'] = ans_list
			return HttpResponse(json.dumps(grp_data), content_type='application/json', status=200)
		return HttpResponse("No Answer is related to this question")


#Answer ans its Question
class AnswersList(View):

	#get: to fetch answer and its question
	def get(self, request, id):
		answer = Answer.objects.get(id=id)
		if answer:
			data = {}
			data['answer_id'] = id
			data['description'] = answer.description
			ques = Question.objects.get(id=answer.question_id)
			data['question_id'] = ques.id
			data['question_title'] = ques.title
			return HttpResponse(json.dumps(data), content_type='application/json', status=200)
		return HttpResponse("No Answer Avaiable")		

#for popular question
class PopularQuestion(View):

	#get: to fetch the popular question 
	def get(self, request):
		try:
			popular = Question.objects.all().order_by('-upvote')
			question_list = []
			upv = popular[0].upvote
			for item in popular:
				if upv == item.upvote:
					question_list.append(item.description)
				else:
					break	
			return HttpResponse(json.dumps(question_list), content_type='application/json', status=200)
		except ObjectDoesNotExist:
			return HttpResponse("No Question Avaiable")












