#Project name: Assignment
this project is similar stackoverflow which have questions and thier answers and votes to both.

```
API- "stackoverflow" This api is simply collect all api in "app-Stackoverflow" urls      
```

##App name: Stackoverflow

Models:
	 - Parent (,description,upvote,downvote,created_by,created_at,updated_by,updated_at)	
	 - Question (title,tags)
	 - Answer (question) 

API's:
	- "question" this api use for add a question to Stackoverflow.
	- "question/(?P<id>\d+)" this api is  use for fetching ,updating ,deleting the details of question.
	- "answer/(?P<id>\d+" this api is use for adding ,fetching ,updating ,deleting the details of answer.
	- "tags/(?P<tag>\w+" this api is use for collecting the all the question related to tag.
	- "questionupvote/(?P<id>\d+" this api is use for giving upvote to a question.
	- "questiondownvote/(?P<id>\d+"	this api is use for giving downvote to a quetsion.
	- "answerupvote/(?P<id>\d+" this api is use for giving upvote to a answer.
	- "answerdownvote/(?P<id>\d+" this api is use for giving downvote to a answer.
	- "answers/(?P<id>\d+" this api is use for collecting all the answer to a question.	


